package com.aswdc.datatransfer;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity_main);

        tv = findViewById(R.id.text);

        Intent intent = getIntent();
        String concatTemp = intent.getStringExtra("message");
        tv.setText(concatTemp);

    }
}
