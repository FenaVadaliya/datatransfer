package com.aswdc.datatransfer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    EditText etfirstname, etlastname;
    TextView txtdisplay;
    Button btnsubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etfirstname = findViewById(R.id.etActFirstName);
        etlastname = findViewById(R.id.etActLastName);
        txtdisplay = findViewById(R.id.txtActDisplay);

        btnsubmit = findViewById(R.id.ActButton);

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String firstname = etfirstname.getText().toString();
                String lastname = etlastname.getText().toString();
                String concatTemp = firstname + " " + lastname;
                txtdisplay.setText(concatTemp);

                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                i.putExtra("message", concatTemp);
                startActivity(i);
            }
        });
    }
}